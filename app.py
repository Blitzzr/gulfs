import folium
from folium.plugins import MarkerCluster
import pandas as pd
from flask import Flask

db = [['Albay Gulf', 13.188, 123.9308, 'Philippines'],
 ['Gulf Islands', 48.95, -123.53333333, 'Canada'],
 ['Gulf of Aden', 12, 48, 'International'],
 ['Gulf of Alaska', 57, -144, 'International'],
 ['Ambracian Gulf', 38.9725, 20.96916667, 'Greece'],
 ['Amundsen Gulf', 71.00027778, -124.00277778, 'Canada'],
 ['Gulf of Aqaba', 28.75, 34.75, 'Saudi Arabia'],
 ['Argolic Gulf', 37.33333333, 22.91666667, 'Greece'],
 ['Milagros, Masbate', 12.2192, 123.5089, 'Philippines'],
 ['Gulf of Bahrain', 25.69722222, 50.53416667, 'Qatar'],
 ['Gulf of Boothia', 70.66666667, -91, 'Canada'],
 ['Gulf of Bothnia', 63, 20, 'International'],
 ['Gulf of Burgas', 42.5, 27.58333333, 'Bulgaria'],
 ['Gulf of Cádiz', 36.83333333, -7.16666667, 'International'],
 ['Gulf of California', 28, -112, 'International'],
 ['Cambridge Gulf', -14.9, 128.23333333, 'Australia'],
 ['Gulf of Carpentaria', -14, 139, 'International'],
 ['Gulf of Cazones', 21.9302778, -81.2875, 'Cuba'],
 ['Gulf of Corinth', 38.2, 22.5, 'Greece'],
 ['Gulf of Corryvreckan', 56.15358056, -5.70705833, 'United Kingdom'],
 ['Davao Gulf', 6.5, 125.9763, 'Philippines'],
 ['Exmouth Gulf', -22.16666667, 114.3, 'Australia'],
 ['Gulf of the Farallones', 37.7504857, -122.7516535, 'United States'],
 ['Gulf of Finland', 59.83333333, 26, 'International'],
 ['Gulf of Fonseca', 13.25, -87.75, 'El Salvador'],
 ['Gulf of Gabès', 34, 10.41666667, 'Tunisia'],
 ['Gulf of Genoa', 44.16666667, 8.91666667, 'International'],
 ['Gulf of Guinea', 0, 0, 'Soul Buoy'],
 ['Gulf of Hammamet', 36.08333333, 10.75, 'Tunisia'],
 ['Hauraki Gulf', -36.33333333, 175.08333333, 'New Zealand'],
 ['Gulf of Honduras', 16.1514, -88.2517, 'Belize'],
 ['Gulf of İzmir', 38.48333333, 26.81666667, 'Türkiye'],
 ['Gulf of Khambhat', 21.5, 72.5, 'India'],
 ['Gulf of Kuşadası', 37.84138889, 27.13361111, 'Türkiye'],
 ['Gulf of Kutch', 22.6, 69.5, 'India'],
 ['Lagonoy Gulf', 13.59, 123.6728, 'Philippines'],
 ['Leyte Gulf', 10.8333, 125.4167, 'Visayas'],
 ['Lingayen Gulf', 16.28333333, 120.2, 'Philippines'],
 ['Gulf of Lion', 42.99638889, 4.00027778, 'International'],
 ['Gulf of Maine', 43, -68, 'International'],
 ['Malian Gulf', 38.86666667, 22.63333333, 'Greece'],
 ['Gulf of Mannar', 8.47, 79.02, 'International'],
 ['Gulf of Mexico', 25, -90, 'International'],
 ['Gulf of Morbihan', 47.6, -2.8, 'France'],
 ['Gulf of Nicoya', 9.8, -84.8, 'Costa Rica'],
 ['Gulf of Odessa', 46.4683155, 30.7630609, 'Ukraine'],
 ['Gulf of Oman', 25, 58, 'International'],
 ['Gulf of Oristano', 39.83777778, 8.48444444, 'Italy'],
 ['Gulf of Panama', 8.08642, -79.28284, 'Panama'],
 ['Panay Gulf', 10.25, 122.2486, 'Visayas'],
 ['Persian Gulf', 26, 52, 'International'],
 ['Peter the Great Gulf', 42.632, 131.779, 'Russia'],
 ['Ragay Gulf', 13.5839, 123.0654, 'Philippines'],
 ['Gulf of Riga', 57.75, 23.5, 'Estonia'],
 ['Gulf of Roses', 42.183, 3.183, 'Spain'],
 ['Gulf of St. Lawrence', 48.6, -61.4, 'Canada'],
 ['Gulf St Vincent', -35, 138, 'Australia'],
 ['Saronic Gulf', 37.78333333, 23.38333333, 'Greece'],
 ['Gulf of Sidra', 31.5, 18, 'Libya'],
 ['Spencer Gulf', -34.41666667, 136.75, 'Australia'],
 ['Gulf of Suez', 28.75, 33, 'Egypt'],
 ['Gulf of Taranto', 39.885, 17.27694444, 'Italy'],
 ['Strait of Tartary', 52.18333333, 141.61666667, 'Russia'],
 ['Gulf of Thailand', 9.5, 102, 'Gulf of Thailand'],
 ['Gulf of Tonkin', 19.75, 107.75, 'International'],
 ['Gulf of Tunis', 37, 10.5, 'Tunisia'],
 ['Gulf of Varna', 43.2, 27.93333333, 'Bulgaria'],
 ['Gulf of Venice', 45.31666667, 13, 'International'],
 ['Moro Gulf', 6.85, 123, 'Mindanao']]


df = pd.DataFrame(db, columns=["Name", "Lat", "Lon", "Country"])


m = folium.Map(location=(37, 10), zoom_start=5)
folium.TileLayer('stamenwatercolor').add_to(m)

marker_cluster = MarkerCluster().add_to(m)
for name, country, lon, lat in df[["Name", "Country", "Lon", "Lat"]].values:
  message = "<h3>%s</h3><br />%s"%(name, country)
  popup=folium.Popup(message, max_width=len(message)*20)
  marker = folium.Marker(popup=popup)
  marker.location = (lat, lon)
  marker.add_to(marker_cluster)

app = Flask(__name__)


@app.route('/')
def index():
    return m._repr_html_()

if __name__ == '__main__':
    app.run(debug=False, host="0.0.0.0")
